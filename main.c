#include "head.h"

int main(int argc, char **argv){
    int cantidadHilos;
    int inicio = 0;
    int cantidadElementosArray = 0;
    ptrRango rangoArray;
    pthread_t * arrayHilos;

    if(validarInput(argc, argv)){
        numeroBuscado = atoi(argv[ 1 ]);
        cantidadHilos = atoi(argv[ 2 ]);
        if( cantidadHilos ){
            pthread_mutex_init(&lock, NULL);

            if(cantidadHilos > MAX){
                cantidadHilos = MAX;
                printf("ADVERTENCIA: La cantidad de hilos ha superado la longitud del vector, se tomara el valor siguiente:%d\n", cantidadHilos);
            }
            if(numeroBuscado <= 0 || numeroBuscado >= 10){
                printf("ADVERTENCIA: El programa solo genera numeros del 0 al 9\n");

            }
            rellenarArray(arrayNumeros);
            datosBusqueda = (ptrInfoDato)malloc(sizeof(infoDato));
            datosBusqueda->indice = 0;
            datosBusqueda->status = 0;
            datosBusqueda->numHilo = 0;

            imprimir(arrayNumeros);


            arrayHilos = (pthread_t *)malloc(sizeof(pthread_t) * cantidadHilos);
            rangoArray = (ptrRango)malloc(sizeof(rango) * cantidadHilos);

            int tamanhoArr = MAX / cantidadHilos;
            int restante = MAX - tamanhoArr * cantidadHilos;

            for (int inicioIndice = 0, finIndice = tamanhoArr, i = 0;inicioIndice < MAX;inicioIndice = finIndice, finIndice = inicioIndice + tamanhoArr, i++)
            {
                if (restante) {
                    finIndice++;
                    restante--;
                }
                rangoArray[ i ].numHilo = i + 1;
                rangoArray[ i ].inicio = inicioIndice;
                rangoArray[ i ].fin = finIndice - 1;
                printf("Numero de Hilo: %d\tindice Desde: %d\tindice Hasta:%d\n",rangoArray[ i ].numHilo, rangoArray[ i ].inicio, rangoArray[ i ].fin);

            }
            for(int i = 0; i < cantidadHilos; i++){
                pthread_create(&arrayHilos[ i ], NULL, funcionHilo, (void *)&rangoArray[ i ]);
            }

            for (int i = 0; i < cantidadHilos; i++){
                pthread_join(arrayHilos[ i ], NULL); //se espera a cada hilo
            }
            if(datosBusqueda->status){
                printf("Se ha encontrado el elemento %d\nIndice:%d\nHilo Numero:%d\n",numeroBuscado, datosBusqueda->indice, datosBusqueda->numHilo);
            }
            else{
                printf("No se ha encontrado el valor en el array\n");
            }
            free(datosBusqueda);
            free(arrayHilos);
            free(rangoArray);
        }
        else{
            printf("ERROR: cantidad de hilos = 0\n");
        }
    }
    pthread_mutex_destroy(&lock);

    return 0;
}
