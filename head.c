
#include "head.h"

int validarInput(int cantidadInput, char **argumentos ){
    int indice = 1;
    int contadorNumeros = 0;

    int bandera = 1;
    if(cantidadInput == 3){
        while (indice < cantidadInput && bandera){
            int j = 0;
            while (argumentos[ indice ][ j ] != '\0' && bandera){
                if (!isdigit(argumentos[ indice ][ j ])){
                    bandera = 0;
                }
                else{
                    j++;
                }
            }
            if(bandera){
                contadorNumeros++;
                indice++;
            }

        }
        if(contadorNumeros == cantidadInput - 1){
            return 1;
        }
        else{
            printf("ERROR: El argumento numero:%d no es numerico\n", indice);
            return 0;
        }
    }
    else{
        printf("ERROR: Cantidad invalida de argumentos\n");
        return 0;
    }
}
void rellenarArray(int * array){
    srand(time(NULL));
    for(int i = 0; i < MAX; i++){
        array[ i ] = rand() % 10;
    }
}
void imprimir(int * array){
    printf("\n|indice|");
    for(int i = 0; i < MAX; i++){
        printf("%3d|", i);
    }
    printf("\n|valor|");
    for(int i = 0; i < MAX; i++){
        printf("%3d|", array[ i ]);
    }
    printf("\n");
}

void *funcionHilo(void *argumento){
    ptrRango rango1 = (ptrRango) argumento;
    for(int i = rango1->inicio; i <= rango1->fin; i++){
        if(arrayNumeros[ i ] == numeroBuscado){
            pthread_mutex_lock(&lock);
            if(!datosBusqueda->status){
                datosBusqueda->status = 1;
                datosBusqueda->indice = i;
                datosBusqueda->numHilo = rango1->numHilo;
                pthread_mutex_unlock(&lock);
                pthread_exit(0);
            }
            else{
                pthread_mutex_unlock(&lock);
                pthread_exit(0);
            }
        }

    }
    return NULL;
}
