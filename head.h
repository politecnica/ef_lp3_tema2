#ifndef HEAD
#define HEAD

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <pthread.h>
#include <ctype.h>
#include <math.h>

#define MAX 11

struct rango{
    int inicio;
    int fin;
    int numHilo;
};

typedef struct rango rango;
typedef rango *ptrRango;

struct infoDato{
    short status;
    int indice;
    int numHilo;
};
typedef struct infoDato infoDato;
typedef infoDato * ptrInfoDato;

void rellenarArray(int * array);
void imprimir(int * array);
void *funcionHilo(void *argumento);
void imprimirLineas();
void imprimirLineas2();
int validarInput(int argc, char **argv );

int arrayNumeros[ MAX ];
int numeroBuscado;
ptrInfoDato datosBusqueda;
pthread_mutex_t lock;
int nivelesArbol;

void initCut(ptrRango rango, int cantidadHilos);
void cortarArray(ptrRango rango, int low, int high, int nivelActual, int indiceRangoI,int indiceRangoD);

#endif // HEAD
